package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.entity.Instructor;
import com.luv2code.hibernate.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        // create a session
        Session session = factory.getCurrentSession();

        try {

            // start transaction
            session.beginTransaction();

            // get instructor by primary key / Id
            int theId = 2;
            Instructor tempInstructor = session.get(Instructor.class, theId);

            System.out.println("found instructor: " + tempInstructor);

            // delete the instructors
            if (tempInstructor != null){
                System.out.println("Deleting: " + tempInstructor);

                // eliminar o instructor
                // NOTE: will ALSO delete associated "details" object
                // because of the CascadeType.ALL
                session.delete(tempInstructor);
            }


            // commit transaction
            session.getTransaction().commit();

            System.out.println("Success!");
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            factory.close();
        }
    }

}
