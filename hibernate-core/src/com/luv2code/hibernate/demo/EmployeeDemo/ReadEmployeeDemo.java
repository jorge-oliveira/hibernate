package com.luv2code.hibernate.demo.EmployeeDemo;

import com.luv2code.hibernate.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ReadEmployeeDemo {

    public static void main(String[] args) {

        // Criacao de uma sessao factory
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        // establece a ligacao
        System.out.println("A estabelecer a Sessao.");
        Session session = sessionFactory.getCurrentSession();

        try {

            // inicio da transacao
            session.beginTransaction();

            // criacao de um empregado para adicionar o id para ser identificado
            Employee theEmployee = new Employee();
            theEmployee.setId(1);

            // retrieve student based on the id: primary key
            System.out.println("\nGetting student with id: " + theEmployee.getId());

            // captura a informacao relacionada com o id informado
            Employee myEmployee = session.get(Employee.class, theEmployee.getId());

            // mostra os dados capturados
            System.out.println("Get complete: " + myEmployee);

            // gravar o empregado
            session.getTransaction().commit();

            System.out.println("Feito...");

        } finally {
            System.out.println("Sessao fechada!.");
            session.close();
        }
        {

        }
    }
}
