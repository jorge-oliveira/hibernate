package com.luv2code.hibernate.demo.EmployeeDemo;

import com.luv2code.hibernate.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class QueryEmployeeDemo {

    public static void main(String[] args) {

        // Criacao de uma sessao factory
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        // establece a ligacao
        System.out.println("A estabelecer a Sessao.");
        Session session = sessionFactory.getCurrentSession();

        try {

            // inicio da transacao
            session.beginTransaction();

            // criacao de um empregado para adicionar o id para ser identificado
            Employee theEmployee = new Employee();
            theEmployee.setId(1);

            // retrieve student based on the id: primary key
            System.out.println("\nGetting student with id: " + theEmployee.getId());

            // captura a informacao relacionada com o id informado
            List<Employee> myEmployee = session.createQuery("from Employee e where e.company='Companhia' ").getResultList();

            // mostra os dados capturados
            for (Employee myEmployees : myEmployee ) {
                System.out.println("\nLista de empregados: " +myEmployees);
            }

            // executa a transacao
            session.getTransaction().commit();

            System.out.println("Feito...");

        } finally {
            System.out.println("Sessao fechada!.");
            session.close();
        }
        {

        }
    }
}
