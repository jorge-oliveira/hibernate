package com.luv2code.hibernate.demo.EmployeeDemo;

import com.luv2code.hibernate.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteEmployeeDemo {

    public static void main(String[] args) {

        // Criacao de uma sessao factory
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        // establece a ligacao
        System.out.println("A estabelecer a Sessao.");
        Session session = sessionFactory.getCurrentSession();

        try {

            // inicio da transacao
            session.beginTransaction();

            // elimina o id =1
            session.createQuery("delete from Employee where id=1").executeUpdate();

            // executa a transacao
            session.getTransaction().commit();

            System.out.println("Feito...");

        } finally {
            System.out.println("Sessao fechada!.");
            session.close();
        }
        {

        }
    }
}
