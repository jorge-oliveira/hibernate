package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.text.ParseException;

import static com.luv2code.hibernate.demo.DateUtils.parseDate;


public class PrimaryKeyDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try{
            // use the session object to save Java object

            // create 3 student objects
            System.out.println("Creating 3 student objects");
            Student tempStudent1 = new Student("Jorge" , "Oliveira", "email@teste.com", parseDate("12-12-12"));
            Student tempStudent2 = new Student("Floki" , "Viking", "email@teste.com", parseDate("12-12-12"));
            Student tempStudent3 = new Student("Ragnar" , "Viks", "email@teste.com", parseDate("12-12-12") );

            // start a transaction
            session.beginTransaction();

            // save the student object
            System.out.println("Saving the students...");
            session.save(tempStudent1);
            session.save(tempStudent2);
            session.save(tempStudent3);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!.");

        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Closing...");
            factory.close();
        }
    }
}
