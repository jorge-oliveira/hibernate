package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class QueryStudentDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try{
            // use the session object to save Java object

            // start a transaction
            session.beginTransaction();

            // query students
            List<Student> theStudents = session.createQuery("from Student").getResultList();

            // display the students
            displayTheStudents(theStudents);

            // query studens: lastName='oliveira'
            theStudents = session.createQuery("from Student s where s.lastName='oliveira' ").getResultList();

            // display the studens
            System.out.println("\n\nStudens who have last name oliveira");
            displayTheStudents(theStudents);

            // query studens: lastName='oliveira' OR firsName='floki'
            theStudents = session.createQuery("from Student s where "
                    + "s.lastName='oliveira' OR s.firstName='floki' ").getResultList();

            // display the studens
            System.out.println("\n\nStudens who have last name oliveira or firstname floki");
            displayTheStudents(theStudents);

            // query students where email LIKE '%teste.com'
            theStudents = session.createQuery("from Student s where "
                    + "s.email LIKE '%gmail.com' ").getResultList();

            // display the studens
            System.out.println("\n\nStudens who have email ends with gmail.com");
            displayTheStudents(theStudents);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!.");

        }finally {
            System.out.println("Closing...");
            factory.close();
        }
    }

    // metodo criado de forma automatica usando o refactor
    private static void displayTheStudents(List<Student> theStudents) {
        for (Student tempStudent: theStudents ) {
            System.out.println(tempStudent);
        }
    }
}
