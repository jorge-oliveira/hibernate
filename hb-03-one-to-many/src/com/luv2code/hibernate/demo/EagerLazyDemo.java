package com.luv2code.hibernate.demo;

import com.luv2code.hibernate.entity.Course;
import com.luv2code.hibernate.entity.Instructor;
import com.luv2code.hibernate.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class EagerLazyDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        // create a session
        Session session = factory.getCurrentSession();

        try {

             // start transaction
            session.beginTransaction();

            // get the instructor from db
            int theId = 1;

            Instructor tempInstructor = session.get(Instructor.class, theId);

            System.out.println("luvcode: Instructor: " + tempInstructor);

            // get course for the instructor
            System.out.println("Courses: " + tempInstructor.getCourses());


            // commit transaction
            session.getTransaction().commit();

            // close the session
            session.close();


            // since courses are lazy loaded... this should fail
            System.out.println("\nluv2code: The session is now closed!\n");
            // option 1: call getter method while session is open



            // get course for the instructor
            System.out.println("Courses: " + tempInstructor.getCourses());


            System.out.println("luv2code: Success!");

        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            factory.close();
        }
    }

}
