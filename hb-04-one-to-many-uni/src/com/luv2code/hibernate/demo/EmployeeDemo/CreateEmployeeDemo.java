package com.luv2code.hibernate.demo.EmployeeDemo;

import com.luv2code.hibernate.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateEmployeeDemo {

    public static void main(String[] args) {

        // Criacao de uma sessao factory
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        // establece a ligacao
        System.out.println("A estabelecer a Sessao.");
        Session session = sessionFactory.getCurrentSession();

        try {

            // criar um novo empregado
            System.out.println("A criar um empregado... ");
            Employee theEmployee = new Employee("Ricardo", "Ribeiro", "Companhia de testes");

            // inicio da transacao
            session.beginTransaction();

            // salvar o empregado
            System.out.println("Empregado a ser gravado... " );
            session.save(theEmployee);

            // gravar o empregado
            session.getTransaction().commit();

            System.out.println("Empregado gravado.");

        } finally {
            System.out.println("Sessao fechada!.");
            session.close();
        }
        {

        }
    }
}
